import React from 'react';
import { View, Text } from 'react-native';
import { Card, Button, CheckBox, ListItem } from 'react-native-elements'

export class BluetoothDetail extends React.Component {
    constructor() {
        super();

        this.state = {
            checked: false
        };
    }

    render() {
        const device = this.props.navigation.state.params.rowData;

        const list = [
            {
              name: 'David Barnes',
              avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
              subtitle: 'Checked-in: Less than a minute ago',
              timeIn:"<1"
            },
            {
              name: 'Daniel Pack',
              avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
              subtitle: 'Checked-in: 15 minutes ago',
              timeIn:"15"
            },
          ];

        return (
            <View>
                <Card 
                    title={ 'Weight Room' } //device.uuid
                    resizeMode = "cover"
                    image={{ uri:'https://life-cdn.global.ssl.fastly.net/life/wp-content/uploads/2017/09/Common-Weight-Room-Mistakes_.jpg' }}>
                    <Text style={{ fontWeight: 'bold', marginBottom: 10, textAlign: 'center' }}>
                        Currently Checked-in Members:
                    </Text>

                    {
                        list.map((l, i) => (
                        <ListItem
                            key={i}
                            leftAvatar={{ source: { uri: l.avatar_url } }}
                            title={l.name}
                            subtitle={l.subtitle}
                        />
                        ))
                    }
                    
                    
                    {/* <Button
                        rightIcon={{name: 'build'}}
                        backgroundColor='#03A9F4'
                        buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                        title='VIEW NOW' /> */}
                </Card>

                {/* <CheckBox
                        center
                        title='Click Here'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.checked}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        onPress={() => console.log("this.checked")}
                    /> */}
            </View>
            // <View>
            //     <Text>BluetoothDetail</Text>
            //     <Text>{ this.props.navigation.state.params.rowData.rssi }</Text>
            //     <Text>{ this.props.navigation.state.params.rowData.proximity }</Text>
            //     <Text>{ this.props.navigation.state.params.rowData.accuracy }</Text>
            // </View>
        );
    }
}