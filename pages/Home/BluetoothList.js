import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  ListView,
  DeviceEventEmitter
} from 'react-native';
import Beacons from 'react-native-ibeacons';
import { ListItem } from 'react-native-elements'

const UUID = '61687109-905F-4436-91F8-E602F514C96D';

export class BluetoothList extends React.Component {
  constructor(props) {
    super(props);

    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.state = {
      bluetoothState: '',
      identifier: '',
      uuid: UUID,
      dataSource: ds.cloneWithRows([])
    };
  }

  componentWillMount() {
    Beacons.requestWhenInUseAuthorization();

    const region = {
      identifier: this.state.identifier,
      uuid: this.state.uuid
    };

    Beacons.startRangingBeaconsInRegion(region);
    Beacons.startUpdatingLocation();
  }

  componentDidMount() {
    this.beaconsDidRange = DeviceEventEmitter.addListener('beaconsDidRange', (data) => {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(data.beacons)
      });
    });
  }

  render() {
    const { dataSource } = this.state;

    return (
      <View style={styles.container}>
        <ListView
          dataSource={dataSource}
          enableEmptySections={true}
          renderRow={(rowData) => this.renderRow(rowData)}
        />
      </View>
    );
  }

  renderRow(rowData) {
    return (
      <ListItem
        roundAvatar
        title={ rowData.minor == 34263 ? "Front Entry (checkin)" : rowData.minor == 34295 ? "Weight Room" : rowData.minor == 34296 ? "Pool" : ""}
        subtitle={ "Distance: " +rowData.accuracy.toFixed(2) + " (" + rowData.proximity + ")" }
        leftIcon={{ name: 'bluetooth' }}
        onPress={() => this.props.navigation.navigate('DeviceDetail', { rowData:rowData })}
      />
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    padding: 0,
    margin: 0,
    backgroundColor: '#F5FCFF',
  }, welcome: {
    fontWeight: 'bold'
  }
});