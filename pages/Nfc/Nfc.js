import React from 'react';
import { Text, ListView, View, Alert, DeviceEventEmitter } from 'react-native';
import Beacons from 'react-native-ibeacons';

const UUID = '61687109-905F-4436-91F8-E602F514C96D';

export class Nfc extends React.Component {
  constructor() {
    super();

    this.state = {
        identifier: '',
        uuid: UUID,
        nfcBeacon: {},
        isCheckedIn: false
      };
  }

  componentDidUpdate() {
  
  }

  componentWillMount() {
    Beacons.requestWhenInUseAuthorization();

    const region = {
      identifier: this.state.identifier,
      uuid: this.state.uuid
    };

    Beacons.startRangingBeaconsInRegion(region);
    Beacons.startUpdatingLocation();
  }

  componentDidMount() {
    this.beaconsDidRange = DeviceEventEmitter.addListener('beaconsDidRange', (data) => {
        data.beacons.forEach(element => {
            if(element.minor === 34263) {
                this.setState({
                    nfcBeacon:element
                });
            }
        });
    });
  }

  render() {
    const { nfcBeacon, isCheckedIn } = this.state;

    if((nfcBeacon.accuracy < .5 && nfcBeacon.accuracy != -1) && !isCheckedIn) {
        Alert.alert('Checked-in success!', "Member Id: ".concat(this.state.barCodeData), [
            {text: 'OK', onPress: () => console.log("ok")},
            {text: 'Undo check-in', onPress: () => console.log("undo")},
          ],{ 
            cancelable: false 
          }
        );

        this.setState({
            isCheckedIn:true
        });

        return(
            <Text>Checked-in!!</Text>
        );
    } else if(isCheckedIn) {
        return (
            <View style={{flex: 1}}>
                <Text>Currently Checkedin</Text>
            </View>
            );
    } else {
        return (
        <View style={{flex: 1}}>
            <Text>{nfcBeacon.minor}</Text>
            <Text>{nfcBeacon.accuracy}</Text>
        </View>
        );
    }
  }

}