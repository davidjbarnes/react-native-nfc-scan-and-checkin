import React from 'react';
import { View, Button, StyleSheet } from 'react-native';
import Canvas from 'react-native-canvas';
import { Dimensions } from 'react-native'

class Beacon {
  constructor(name, location, height, width, fillColor, rssi) {
    this.name = name;
    this.location = {
      x:location.x,
      y:location.y,
    };
    this.height = height;
    this.width = width;
    this.centroid = {
      x:this.location.x + (this.width / 2),
      y:this.location.y + (this.height / 2)
    };
    this.fillColor = fillColor;
    this.rssi = rssi;
  }
}

export class Map extends React.Component {
  beaconList = [];

  constructor() {
    super();

    this.beaconList.push(new Beacon('Upper Left', { x:0, y:0 }, 25, 25, 'red', 200));
    this.beaconList.push(new Beacon('Upper Right', { x:350, y:0 }, 25, 25, 'purple', 200));
    this.beaconList.push(new Beacon('Lower Center', { x:187, y:300 }, 25, 25, 'green', 200));
  }

  render() {
    return (
      <View style={ styles.container }>
        <Canvas ref={ this.handleCanvas } onPress={ this.onCanvasClick } />
        <Button
          title="Go to Details"
          onPress={() => this.props.navigation.navigate('MapDetail')}
        />
      </View>
    );
  }

  onCanvasClick = (canvas) => {
    console.log("click");
  }

  drawObject(ctx, objectType, object) {
    console.log("Drawing object: ");
    console.log(object);

    if(objectType === 'beacon') {
      ctx.fillStyle = object.fillColor;
      ctx.fillRect(object.location.x, object.location.y, object.width, object.height);
    } else if(objectType === 'beacon-with-signal') {
      ctx.fillStyle = object.fillColor;
      ctx.fillRect(object.location.x, object.location.y, object.width, object.height);

      ctx.beginPath();
      ctx.strokeStyle = object.fillColor;
      ctx.arc(object.centroid.x, object.centroid.y, object.rssi, 0, 2 * Math.PI);
      ctx.stroke();
    } else {
      console.log("Unsupported objectType");
    }
  }

  handleCanvas = (canvas) => {
    var { height, width } = Dimensions.get('window');

    console.log("Dimensions: (w*h)", width, height);

    canvas.width = width;
    canvas.height = height;

    const ctx = canvas.getContext('2d');



    // var { height, width } = Dimensions.get('window');

    // console.log("Dimensions: (w*h)", width, height);

    // canvas.width = width;
    // canvas.height = height;

    // const ctx = canvas.getContext('2d');

    // //Draw canvas BG
    // ctx.fillStyle = 'grey';
    // ctx.fillRect(0, 0, canvas.width, canvas.height);

    // //Draw beacons
    // this.beaconList.forEach((beacon) => this.drawObject(ctx, 'beacon-with-signal', beacon));
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    padding: 0,
    margin: 0,
    backgroundColor: '#F5FCFF',
  },
  headerText: {
    fontWeight: 'bold',
    paddingBottom: 8,
  }
});