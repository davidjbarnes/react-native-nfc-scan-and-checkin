import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

export class MapDetail extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Map Detail</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    padding: 0,
    margin: 0,
    backgroundColor: '#F5FCFF',
  },
  headerText: {
    fontWeight: 'bold',
    paddingBottom: 8,
  }
});