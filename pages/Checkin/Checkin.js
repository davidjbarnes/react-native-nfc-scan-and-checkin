import React from 'react';
import { StyleSheet, Text, ListView, View, Alert } from 'react-native';
import Camera from 'react-native-camera';

export class Checkin extends React.Component {
  constructor() {
    super();

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      barCodeData: null,
      checkedInList: ds.cloneWithRows([]),
      onCodeRead: this.onCodeRead
    };
  }

  componentDidUpdate() {
  
  }

  componentWillMount() {
    
  }

  componentDidMount() {

  }

  onCodeRead = (e) => {
    this.setState({
      onCodeRead: undefined,
      barCodeData: e.data
    });

    //https://api.upcitemdb.com/prod/trial/lookup?upc=50428416044

    fetch('https://jsonplaceholder.typicode.com/users').then((response) => response.json()).then((responseJson) => {
      this.setState({
        checkedInList: this.state.checkedInList.cloneWithRows(responseJson),
      });

      Alert.alert('Checked-in success!', "Member Id: ".concat(this.state.barCodeData), [
          {text: 'OK', onPress: () => this.onCheckinConfirmed()},
          {text: 'Undo check-in', onPress: () => this.onCheckinRollback()},
        ],{ 
          cancelable: false 
        }
      );
    }).catch((error) => {
      console.error(error);
    });
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <Camera style={{ flex: 1 }}
          ref={cam => this.camera=cam}
          aspect={Camera.constants.Aspect.fill}
          onBarCodeRead={this.state.onCodeRead}>
        </Camera>
        <ListView
          dataSource={this.state.checkedInList}
          renderRow={(rowData) => <Text>{rowData.name}</Text>}
          enableEmptySections={true}
        />
      </View>
    );
  }

  onCheckinConfirmed = () => {
    this.setState({
      barCodeData: null,
      onCodeRead: this.onCodeRead
    });
  }

  onCheckinRollback = () => {
    this.setState({
      barCodeData: null,
      onCodeRead: this.onCodeRead
    });

    alert('Check-in has been undone!');
  }
}