import React from 'react';
import { Icon } from 'react-native-elements'
import { TabNavigator, TabBarBottom, StackNavigator } from 'react-navigation';

import { Map } from './pages/Map/Map';
import { MapDetail } from './pages/Map/MapDetail';
import { BluetoothList } from './pages/Home/BluetoothList';
import { BluetoothDetail } from './pages/Home/BluetoothDetail';
import { Checkin } from './pages/Checkin/Checkin';
import { Nfc } from './pages/Nfc/Nfc';

//disables yellow warnings within RN
console.disableYellowBox = true;

const HomeStack = StackNavigator({
  Home: {
    screen: BluetoothList,
    navigationOptions: {
      title: 'Device List',
    },
  },
  DeviceDetail: {
    screen: BluetoothDetail,
    navigationOptions: {
      title: 'Device Detail',
    },
  },
});

const MapStack = StackNavigator({
  Map: {
    screen: Map,
    navigationOptions: {
      title: 'Map',
    },
  },
  MapDetail: {
    screen: MapDetail,
    navigationOptions: {
      title: 'Map Detail',
    },
  },
});

const CheckinStack = StackNavigator({
  Checkin: {
    screen: Checkin,
    navigationOptions: {
      title: 'Check-in',
    },
  },
});

const NfcStack = StackNavigator({
  Nfc: {
    screen: Nfc,
    navigationOptions: {
      title: 'NFC Check-in',
    },
  },
});

const tabs = {
  Home: HomeStack,
  Map: MapStack,
  Checkin: CheckinStack, 
  Nfc: NfcStack
};

export default TabNavigator(tabs,
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;

        let iconName;
        if (routeName === 'Home') {
          iconName = 'bluetooth';
        } else if (routeName === 'Map') {
          iconName = 'map';
        } else if (routeName === 'Checkin') {
          iconName = 'camera';
        } else if (routeName === 'Nfc') {
          iconName = 'toll';
        }

        return <Icon name={iconName} />
      },
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      // activeTintColor: '#dbdbdb',
      inactiveTintColor: 'gray',
    },
    animationEnabled: false,
    swipeEnabled: false,
});