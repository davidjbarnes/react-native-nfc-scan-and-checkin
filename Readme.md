# React Native NFC Scan and Checkin

### Setup
0. $ npm i react-native --save
1. $ git clone https://davidjbarnes@bitbucket.org/davidjbarnes/react-native-nfc-scan-and-checkin.git
2. $ npm i
3. $ react-native run-ios

### Helpful Stuff
- Beacons (currently using Coin Beacon (BC-413)): https://www.bluecats.com/buy-beacons/
- React Native Camera: https://medium.com/react-native-development/react-native-camera-app-with-live-preview-saturation-and-brightness-filters-d34535cc6d14
- Bar Code Lookup: https://www.upcitemdb.com/api/explorer#!/lookup/get_trial_lookup
- Icon Library: https://material.io/tools/icons/?style=baseline

### Other Stuff
![Beacons](https://doc-08-3k-docs.googleusercontent.com/docs/securesc/hau7k197faqn917vbamce3otv5nsts2f/ceavkhpe7qi1uqq2djfhlm635kjfq11v/1534521600000/00317271827477209045/00317271827477209045/17E7P81589MjGuACj706jb2chCtOUfQZ7)